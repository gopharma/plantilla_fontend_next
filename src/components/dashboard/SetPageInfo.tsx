'use client';
import { useEffect } from 'react';
import usePage from '@/hooks/usePage';

const SetPageInfo = ({ pageName, iconPage, breadCrumb }: any) => {
    const { setBreadCrumb, setIconPage, setPageName } = usePage();

    useEffect(() => {
        setPageName(pageName);
        setIconPage(iconPage);
        setBreadCrumb(breadCrumb);
    }, []);

    return null;
};

export default SetPageInfo;
