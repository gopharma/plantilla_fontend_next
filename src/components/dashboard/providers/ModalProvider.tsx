import React, { useEffect } from 'react';
import { Button, Card, Col, Modal, Row } from 'antd';

const ModalProvider = ({ dataProvider, isOpen, onClose }: any) => {
    useEffect(() => {
        console.log('Datos del proveedor:', dataProvider);
    }, [dataProvider]);

    return (
        <Modal
            title={`Detalles del Proveedor`}
            centered
            open={isOpen}
            onOk={onClose}
            onCancel={onClose}
            width="500px" // Ancho reducido
            bodyStyle={{
                maxHeight: 'calc(100vh - 120px)', // Altura máxima adaptada a la pantalla
                overflowY: 'auto', // Habilitar desplazamiento vertical
                padding: '10px',
            }}
            style={{
                top: 20, // Mantener un pequeño margen superior
            }}
            footer={
                <Button type="primary" onClick={onClose}>
                    Cerrar
                </Button>
            }
        >
            <Card bordered={false} style={{ backgroundColor: '#f7f7f7', padding: '8px' }}>
                <Row gutter={[8, 8]} style={{ marginBottom: '4px' }}>
                    <Col span={24}>
                        <h3 style={{ fontSize: '14px', marginBottom: '4px' }}>
                            <b>Datos de Identificación</b>
                        </h3>
                        <hr style={{ margin: '4px 0' }} />
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Razón Social:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.businessName}
                                </h5>
                            </Col>
                        </Row>
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Identificación:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5
                                    style={{ marginBottom: '2px', fontSize: '12px' }}
                                >{`${dataProvider.documentType.identificationType.name}: ${dataProvider.documentType.code} ${dataProvider.identification}`}</h5>
                            </Col>
                        </Row>

                        <h3 style={{ fontSize: '14px', margin: '8px 0 4px 0' }}>
                            <b>Datos de Contacto</b>
                        </h3>
                        <hr style={{ margin: '4px 0' }} />
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col xs={24} sm={8}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Sitio Web:
                                </h5>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.website}
                                </h5>
                            </Col>
                            <Col xs={24} sm={8}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Teléfono:
                                </h5>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.phone}
                                </h5>
                            </Col>
                            <Col xs={24} sm={8}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Correo:
                                </h5>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.email}
                                </h5>
                            </Col>
                        </Row>

                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Estado:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.city.state.name}
                                </h5>
                            </Col>
                        </Row>
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Ciudad:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.city.name}
                                </h5>
                            </Col>
                        </Row>
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Dirección:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.address}
                                </h5>
                            </Col>
                        </Row>

                        <h3 style={{ fontSize: '14px', margin: '8px 0 4px 0' }}>
                            <b>Información Legal</b>
                        </h3>
                        <hr style={{ margin: '4px 0' }} />
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Representante Legal:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5
                                    style={{ marginBottom: '2px', fontSize: '12px' }}
                                >{`${dataProvider.legalRepresentativeFirstName} ${dataProvider.legalRepresentativeLastName}`}</h5>
                            </Col>
                        </Row>
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Fecha de Constitución:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.constitutionDate}
                                </h5>
                            </Col>
                        </Row>

                        <h3 style={{ fontSize: '14px', margin: '8px 0 4px 0' }}>
                            <b>Información Fiscal</b>
                        </h3>
                        <hr style={{ margin: '4px 0' }} />
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Tipo de Contribuyente:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.taxpayer.name}
                                </h5>
                            </Col>
                        </Row>
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Porcentaje:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5 style={{ marginBottom: '2px', fontSize: '12px' }}>
                                    {dataProvider.taxRetentionPercentage.description}
                                </h5>
                            </Col>
                        </Row>
                        <Row gutter={8} style={{ marginBottom: '4px' }}>
                            <Col span={10}>
                                <h5
                                    style={{
                                        fontWeight: 'bold',
                                        marginBottom: '2px',
                                        fontSize: '12px',
                                    }}
                                >
                                    Tipo de Proveedor ISRL:
                                </h5>
                            </Col>
                            <Col span={14}>
                                <h5
                                    style={{ marginBottom: '2px', fontSize: '12px' }}
                                >{`${dataProvider.typePeopleIsrl.type} (${dataProvider.typePeopleIsrl.code})`}</h5>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Card>
        </Modal>
    );
};

export default ModalProvider;
