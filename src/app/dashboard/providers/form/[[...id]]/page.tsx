'use client';
import ButtonBack from '@/components/dashboard/ButtonBack';
import ButtonSubmit from '@/components/dashboard/ButtonSubmit';
import SetPageInfo from '@/components/dashboard/SetPageInfo';
import { createOrUpdate, getActiveList, getOne } from '@/services';
import { AppstoreOutlined } from '@ant-design/icons';
import { Col, Form, FormProps, Input, Row, Select } from 'antd';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

type FieldType = {
    name: string;
    website: string;
    taxpayer: number;
    address: string;
    taxRetentionPercentaje: number;
};

const moduleName = 'providers';

const ModuleForm = ({ params }: any) => {
    const [loader, setLoader] = useState(false);
    const [spinner, setSpinner] = useState(true);
    const [data, setData] = useState({ state: 24, city: 480, clientType: 1, taxpayer: 1 });
    const [optionsStates, setOptionsStates] = useState([]);
    const [optionsCities, setOptionsCities] = useState([]);
    const [optionsClientType, setOptionsClientType] = useState([]);
    const [optionsTaxpayerType, setOptionsTaxpayerType] = useState([]);
    const [form] = Form.useForm();

    const id = params.id ? params.id[0] : null;

    useEffect(() => {
        getData();
    }, []);

    const pageName = `${id ? 'Editar' : 'Nuevo'} Proveedor`;
    const iconPage = <AppstoreOutlined />;
    const breadCrumb = [{ title: 'Maestros' }, { title: 'Proveedors' }, , { title: pageName }];

    const getData = async () => {
        const [getOptionsStates, getOptionsClientType, getOptionsTaxpayerType] = await Promise.all([
            getActiveList('states'),
            getActiveList('client_types'),
            getActiveList('taxpayer_types'),
        ]);

        setOptionsStates(getOptionsStates);
        setOptionsClientType(getOptionsClientType);
        setOptionsTaxpayerType(getOptionsTaxpayerType);

        let getCities;

        if (id) {
            const get = await getOne(moduleName, id);
            get.state = get.city.state.id;
            get.city = get.city.id;
            get.clientType = get.clientType.id;
            get.taxpayer = get.taxpayer.id;
            setData(get);

            getCities = await getActiveList('cities', `?stateId=${get.state}`);
        } else {
            getCities = await getActiveList('cities', `?stateId=${24}`);
        }

        setOptionsCities(getCities);
        setSpinner(false);
    };

    const onFinish: FormProps<FieldType>['onFinish'] = async (values) => {
        setLoader(true);
        const res = await createOrUpdate(moduleName, values, id);

        if (res) {
            setLoader(false);
            return toast.warning(res);
        }

        toast.success(`Proveedor ${id ? 'editado' : 'creado'} con éxito`);
    };

    const handleStateChange = async (value: number) => {
        form.setFieldValue('cities', undefined);
        const getCities = await getActiveList('cities', `?stateId=${value}`);
        setOptionsCities(getCities);
    };

    return spinner ? (
        <p>loading</p>
    ) : (
        <>
            <SetPageInfo pageName={pageName} iconPage={iconPage} breadCrumb={breadCrumb} />
            <ButtonBack />

            <Row>
                <Col span={24}>
                    <Form
                        name="wrap"
                        colon={false}
                        layout="vertical"
                        onFinish={onFinish}
                        initialValues={data}
                    >
                        <Row gutter={32}>
                            <Col span={24} md={8}>
                                <Form.Item<FieldType>
                                    label="Nombre:"
                                    name="name"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col span={24} md={8}>
                                <Form.Item<FieldType>
                                    label="Sitio Web:"
                                    name="website"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col span={24} md={{ span: 8 }}>
                                <Form.Item<FieldType>
                                    label="Tipo de Contribuyente:"
                                    name="taxpayer"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                >
                                    <Select
                                        allowClear
                                        style={{ width: '100%' }}
                                        placeholder="Seleccione"
                                        options={optionsTaxpayerType}
                                        fieldNames={{
                                            label: 'name',
                                            value: 'id',
                                        }}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row gutter={32}>
                            <Col span={24}>
                                <Form.Item<FieldType>
                                    label="Dirección:"
                                    name="address"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row gutter={32}>
                            <Col span={24}>
                                <Form.Item<FieldType>
                                    label="Porcentaje de Retención:"
                                    name="taxRetentionPercentaje"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                        </Row>

                        <ButtonSubmit loader={loader}>
                            {!id ? 'Guardar' : 'Editar'} Proveedor
                        </ButtonSubmit>
                    </Form>
                </Col>
            </Row>
        </>
    );
};

export default ModuleForm;
