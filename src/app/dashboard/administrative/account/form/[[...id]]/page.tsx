'use client';
import ButtonBack from '@/components/dashboard/ButtonBack';
import ButtonSubmit from '@/components/dashboard/ButtonSubmit';
import SetPageInfo from '@/components/dashboard/SetPageInfo';
import { createOrUpdate, getAllData, getOne } from '@/services';
import { AppstoreOutlined } from '@ant-design/icons';
import { Col, Form, FormProps, Input, Row, Select } from 'antd';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import Loading from '@/components/Loading';

type FieldType = {
    nameAccount: string;
    typeAccount: string;
    description: string;
    bank: number;
};

const moduleName = 'administrative/account';

const ModuleForm = ({ params }: any) => {

    const [loader, setLoader] = useState(false);
    const [spinner, setSpinner] = useState(true);
    const [data, setData] = useState({});
    const [currencys, setCurrencys] = useState<{id: number, name: string }[]>([]);

    const id = params.id ? params.id[0] : null;

    const pageName = `${id ? 'Editar' : 'Nuevo'} cuenta`;
    const iconPage = <AppstoreOutlined />;
    const breadCrumb = [{ title: 'Maestros' }, { title: 'Cuentas' }, , { title: pageName }];

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const getData = async () => {
        if (id) {
            const get = await getOne(moduleName, id);
            setData(get);
        }
        setSpinner(false);
    };

    const getCurrencys = async () => {
        const response = await getAllData("administrative/banks","1");
        setCurrencys(response.data);
    };

    useEffect(() => {
        getCurrencys();
        getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onFinish: FormProps<FieldType>['onFinish'] = async (values) => {
        setLoader(true);

        values.typeAccount = String(values.typeAccount);
        values.bank = Number(values.bank);

        if (typeof values.typeAccount === 'string') {
            const res = await createOrUpdate(moduleName, values, id);
            if (res) {
                setLoader(false);
                return toast.warning(res.message);
            }

            toast.success(`Cuenta ${id ? 'editado' : 'creado'} con éxito`);
        }

    };

    const { TextArea } = Input;


    return spinner ? (
        <Loading />
    ) : (
        <>
            <SetPageInfo pageName={pageName} iconPage={iconPage} breadCrumb={breadCrumb} />
            <ButtonBack />

            <Row>
                <Col span={24}>
                    <Form
                        name="wrap"
                        colon={false}
                        layout="vertical"
                        onFinish={onFinish}
                        initialValues={data}
                    >

                        <div style={{
                            display: 'flex',
                            justifyContent: 'center',
                            gap: '20px'
                        }}>
                            <div style={{ width: '20vw' }}>
                            <Form.Item<FieldType>
                                    label="Nombre de cuenta:"
                                    name="nameAccount"

                                >
                                    <Input />

                                </Form.Item>
                                <Form.Item<FieldType>
                                    label="Descripción"
                                    name="description"

                                >
                                    <TextArea rows={4} />

                                </Form.Item>
                            </div>


                            <div style={{ width: '20vw' }}>
                                <Form.Item<FieldType>
                                    label="Tipo de cuenta:"
                                    name="typeAccount"

                                >
                                    <Select
                                        showSearch
                                        style={{ width: 200 }}
                                        placeholder="Selecionar"
                                        optionFilterProp="label"
                                        filterSort={(optionA, optionB) =>
                                            (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
                                        }
                                        options={[
                                            {
                                                value: 'Ahorro',
                                                label: 'Ahorro',
                                            },
                                            {
                                                value: 'Corriente',
                                                label: 'Corriente',
                                            },
                                        ]}
                                    />
                                </Form.Item>
                                <Form.Item<FieldType>
                                    label="Banco:"
                                    name="bank"

                                >
                                    <Select
                                        showSearch
                                        style={{ width: 200 }}
                                        placeholder="Selecionar"
                                        optionFilterProp="label"
                                        filterSort={(optionA, optionB) =>
                                            (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
                                        }
                                        options={currencys.map(currency => ({
                                            value: currency.id,
                                            label: currency.name
                                        }))}
                                    />
                                </Form.Item>
                            </div>
                        </div>



                        <ButtonSubmit loader={loader}>
                            {!id ? 'Guardar' : 'Editar'} Método
                        </ButtonSubmit>
                    </Form>
                </Col>
            </Row >
        </>
    );
};

export default ModuleForm;
