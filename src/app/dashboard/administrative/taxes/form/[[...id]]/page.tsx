'use client';
import ButtonBack from '@/components/dashboard/ButtonBack';
import ButtonSubmit from '@/components/dashboard/ButtonSubmit';
import SetPageInfo from '@/components/dashboard/SetPageInfo';
import { createOrUpdate, getOne } from '@/services';
import { AppstoreOutlined} from '@ant-design/icons';
import { Col, Form, Input, Row } from 'antd';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import Loading from '@/components/Loading';


const moduleName = 'administrative/taxes';



const ModuleForm = ({ params }: any) => {
    const [loader, setLoader] = useState(false);
    const [spinner, setSpinner] = useState(true);
    const [data, setData] = useState({});
 

    const id = params.id ? params.id[0] : null;
    const pageName = `${id ? 'Editar' : 'Nuevo'} Impuesto`;
    const iconPage = <AppstoreOutlined />;
    const breadCrumb = [{ title: 'Administrativo' }, { title: 'Impuestos' }, { title: pageName }];

    // eslint-disable-next-line react-hooks/exhaustive-deps
    const getData = async () => {
        if (id) {
            const get = await getOne(moduleName, id);
            setData(get);
        }
        setSpinner(false);
    };

    useEffect(() => {
        getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    const onFinish = async (values: any) => {
        setLoader(true);

        try {
            const res = await createOrUpdate(moduleName, values, id);
            if (res) {
                setLoader(false);
                return toast.warning(res.message);
            }
            toast.success(`Impuesto ${id ? 'editado' : 'creado'} con éxito`);
        } catch (error) {
            toast.error('Ha ocurrido un error');
        } finally {
            setLoader(false);
        }
    };

   

    return spinner ? (
        <Loading />
    ) : (
        <>
            <SetPageInfo pageName={pageName} iconPage={iconPage} breadCrumb={breadCrumb} />
            <ButtonBack />

            <Row>
                <Col span={24}>
                    <Form
                        name="wrap"
                        colon={false}
                        layout="vertical"
                        onFinish={onFinish}
                        initialValues={data}
                    >
                        <Row gutter={32}>
                            <Col span={24} md={{ span: 8, offset: 6 }}>
                                <div className="container-data-bank">
                                    <Form.Item
                                        label="Moneda:"
                                        name="money"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Este campo es obligatorio',
                                            },
                                        ]}
                                    >
                                        <Input />
                                    </Form.Item>
                                    <Form.Item
                                        label="Simbolo:"
                                        name="symbol"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Este campo es obligatorio',
                                            },
                                        ]}
                                    >
                                        <Input />
                                    </Form.Item>
                                </div>
                            </Col>
                        </Row>

                        <ButtonSubmit loader={loader}>
                            {!id ? 'Guardar' : 'Editar'} Impuesto
                        </ButtonSubmit>
                    </Form>
                </Col>
            </Row>
        </>
    );
};

export default ModuleForm;
