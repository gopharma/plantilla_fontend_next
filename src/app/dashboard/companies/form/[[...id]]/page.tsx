'use client';
import ButtonBack from '@/components/dashboard/ButtonBack';
import ButtonSubmit from '@/components/dashboard/ButtonSubmit';
import SetPageInfo from '@/components/dashboard/SetPageInfo';
import Loading from '@/components/Loading';
import { getFormData } from '@/helpers';
import { createOrUpdate, getOne } from '@/services';
import Filepond from '@/utils/filepond';
import { AppstoreOutlined } from '@ant-design/icons';
import { Col, Form, FormProps, Input, Row } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

type FieldType = {
    name: string;
    email: string;
    phone: string;
    address: string;
    rif: string;
    logo: any;
};

const moduleName = 'companies';

const ModuleForm = ({ params }: any) => {
    const [loader, setLoader] = useState(false);
    const [spinner, setSpinner] = useState(true);
    const [data, setData] = useState({});
    const [files, setFiles] = useState<any[]>([]);

    const id = params.id ? params.id[0] : null;

    useEffect(() => {
        const getData = async () => {
            if (id) {
                const get = await getOne(moduleName, id);
                console.log("datos para editar en /companies", get)
                setData(get);
                if (get.logo) {
                    setFiles([
                        `${process.env.NEXT_PUBLIC_BACKEND_URL}/companies/logos/${get.logo}`,
                    ]);
                }
            }
            setSpinner(false);
        };

        getData();
    }, []);

    const pageName = `${id ? 'Editar' : 'Nueva'} Empresa`;
    const iconPage = <AppstoreOutlined />;
    const breadCrumb = [
        { title: 'Configuración' },
        { title: 'Empresas' },
        ,
        { title: pageName },
    ];

    const onFinish: FormProps<FieldType>['onFinish'] = async (values) => {
        setLoader(true);
        const formData = getFormData(values);
        files[0] && formData.append('logo', files[0].file, files[0].filename);

        const res = await createOrUpdate(moduleName, formData, id);

        if (res) {
            setLoader(false);
            return toast.warning(res);
        }

        toast.success(`Empresa ${id ? 'editada' : 'creada'} con éxito`);
    };

    return spinner ? (
        <Loading />
    ) : (
        <>
            <SetPageInfo
                pageName={pageName}
                iconPage={iconPage}
                breadCrumb={breadCrumb}
            />
            <ButtonBack />

            <Row>
                <Col span={24}>
                    <Form
                        name="wrap"
                        colon={false}
                        layout="vertical"
                        onFinish={onFinish}
                        initialValues={data}
                    >
                        <Row gutter={32}>
                            <Col span={24} md={{ span: 8 }}>
                                <Form.Item<FieldType>
                                    label="Nombre"
                                    name="name"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>

                            <Col span={24} md={{ span: 8 }}>
                                <Form.Item<FieldType>
                                    label="Correo"
                                    name="email"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                        {
                                            type: 'email',
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>

                            <Col span={24} md={{ span: 8 }}>
                                <Form.Item<FieldType>
                                    label="Teléfono"
                                    name="phone"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row gutter={32}>
                            <Col span={24}>
                                <Form.Item<FieldType>
                                    label="RIF"
                                    name="rif"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row gutter={32}>
                            <Col span={24}>
                                <Form.Item<FieldType>
                                    label="Dirección"
                                    name="address"
                                    rules={[
                                        {
                                            required: true,
                                        },
                                    ]}
                                >
                                    <TextArea rows={4} />
                                </Form.Item>
                            </Col>
                        </Row>

                        <Row style={{ marginBottom: '30px' }}>
                            <Col span={24}>
                                <Filepond files={files} setFiles={setFiles} />
                            </Col>
                        </Row>

                        <ButtonSubmit loader={loader}>
                            {!id ? 'Guardar' : 'Editar'} Empresa
                        </ButtonSubmit>
                    </Form>
                </Col>
            </Row>
        </>
    );
};

export default ModuleForm;
