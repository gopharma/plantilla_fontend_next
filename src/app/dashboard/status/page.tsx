'use client';
import ButtonBack from '@/components/dashboard/ButtonBack';
import ButtonEcxel from '@/components/dashboard/ButtonEcxel';
import ButtonForm from '@/components/dashboard/ButtonForm';
import FilterContainer from '@/components/dashboard/FilterContainer';
import SetPageInfo from '@/components/dashboard/SetPageInfo';
import Table from '@/components/dashboard/Table';
import { getData } from '@/services';
import { AppstoreOutlined } from '@ant-design/icons';
import React, { useEffect, useState } from 'react';

const moduleName = 'status';

export interface DataTypeActions {
    id: number;
    status: string;
    module: string;
    color: string;
    isActive: boolean;
    createdAt: Date;
    updatedAt: Date;
    user: {
        id: number;
        name: string;
        email: string;
    };
}

const Page = ({ searchParams }: any) => {
    const [data, setData] = useState<DataTypeActions[]>([]);
    const [totalRows, setTotalRows] = useState<number>(0);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            try {
                const getList = await getData(moduleName, searchParams);
                setData(getList['data']);
                setTotalRows(getList['totalRows']);
            } catch (error) {
                console.error("Error fetching data:", error);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, [searchParams]);

    const pageName = 'Estatus';
    const iconPage = <AppstoreOutlined />;
    const breadCrumb = [{ title: 'Estatus' }, { title: 'Estatus' }];

    const renderColorBox = (color: string) => (
        <div style={{ width: '20px', height: '20px', backgroundColor: color }}></div>
    );

    const columns = [
        {
            title: 'ID',
            index: 'id',
            sorter: true,
            defaultSortOrder: 'descend',
            sortDirections: ['descend', 'ascend', 'descend'],
            fixed: 'left',
            width: 100,
        },
        {
            title: 'Nombre Del Estatus',
            index: 'status',
            width: 200,
        },
        {
            title: 'Módulo',
            index: 'module',
            width: 200,
        },
        {
            title: 'Color',
            index: 'color',
            width: 100,
            render: (color: string) => (
                <div style={{ width: '20px', height: '20px', backgroundColor: color }} />
            ),
        },
        {
            title: 'Registrado por',
            index: 'user',
            responsive: ['md'],
            type: 'object',
            property: 'name',
            width: 200,
        },
        {
            title: 'Fecha registro',
            index: 'createdAt',
            responsive: ['md'],
            type: 'date',
            width: 200,
        },
        {
            title: 'Editado por',
            index: 'userUpdate',
            responsive: ['md'],
            type: 'object',
            property: 'name',
            width: 200,
        },
        {
            title: 'Fecha actualización',
            index: 'updatedAt',
            responsive: ['md'],
            type: 'date',
            width: 200,
        },
        {
            title: 'Estatus',
            index: 'isActive',
            width: 100,
            fixed: 'right',
            type: 'boolean',
            options: [
                {
                    true: 'Activo',
                    value: true,
                    title: 'Activo',
                },
                {
                    false: 'Inactivo',
                    value: false,
                    title: 'Inactivo',
                },
            ],
        },
        {
            title: 'Acciones',
            index: 'actions',
            width: 100,
            fixed: 'right',
            actions: [
                {
                    type: 'edit',
                    route: '/dashboard/status/form/:id',
                },
                {
                    type: 'changeStatus',
                },
            ],
        },
    ];

    return (
        <>
            <SetPageInfo pageName={pageName} iconPage={iconPage} breadCrumb={breadCrumb} />
           

            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <ButtonForm module={moduleName} description={'Nuevo Estatus'} />
                <div style={{ display: 'flex', flexDirection: 'column', gap: '5px', alignItems: 'flex-end' }}>
                    <FilterContainer columns={columns} moduleName={moduleName} />
                    <ButtonEcxel moduleName={moduleName} searchParams={searchParams} />
                </div>
            </div>
            <Table
                columnsProp={columns}
                dataSource={data}
                moduleName={moduleName}
                totalRows={totalRows}
                loading={loading}
            />
        </>
    );
};

export default Page;
